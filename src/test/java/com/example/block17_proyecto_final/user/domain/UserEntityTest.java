package com.example.block17_proyecto_final.user.domain;

import com.example.block17_proyecto_final.country.domain.CountryEntity;
import com.example.block17_proyecto_final.user.domain.enums.Role;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

class UserEntityTest {
    private UserEntity userEntity;
    private UserEntity userEntity2;
    private CountryEntity countryEntity;



    @BeforeEach
    void setUp() {
        userEntity= UserEntity.builder()
                .id(1)
                .email("email@gmail.com")
                .role(Role.USER)
                .country(new CountryEntity())
                .password("Nfoque@6530")
                .fullName("name")
                .isDeleted(false)
                .createdAt(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))
                .build();
        userEntity2= UserEntity.builder()
                .id(1)
                .email("email@gmail.com")
                .role(Role.USER)
                .country(new CountryEntity())
                .password("Nfoque@6530")
                .fullName("name")
                .isDeleted(false)
                .createdAt(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))
                .build();

        countryEntity= CountryEntity.builder()
                .id(1)
                .code("FR")
                .name("Francia")
                .build();
    }

    @Test
    void getAuthorities() {
    }

    @Test
    void getUsername() {
        assertEquals("name",userEntity.getUsername());
    }

    @Test
    void isAccountNonExpired() {
        assertEquals(true,userEntity.isAccountNonExpired());
    }

    @Test
    void isAccountNonLocked() {
        assertEquals(true,userEntity.isAccountNonLocked());
    }

    @Test
    void isCredentialsNonExpired() {
        assertEquals(true,userEntity.isCredentialsNonExpired());
    }

    @Test
    void isEnabled() {
        assertEquals(true,userEntity.isEnabled());
    }

    @Test
    void testEquals() {
        assertEquals(true,userEntity.equals(userEntity2));
    }

    @Test
    void canEqual() {
        assertEquals(true,userEntity.canEqual(userEntity2));
    }

    @Test
    void testHashCode() {
        assertEquals(userEntity.hashCode(),userEntity2.hashCode());
    }

    @Test
    void getId() {
        userEntity.setId(2);
        assertEquals(2,userEntity.getId());

    }

    @Test
    void getFullName() {
        userEntity.setFullName("vvv");
        assertEquals("vvv",userEntity.getFullName());
    }

    @Test
    void getEmail() {
        userEntity.setEmail("aaa@gmailcom");
        assertEquals("aaa@gmailcom",userEntity.getEmail());
    }

    @Test
    void getPassword() {
        userEntity.setPassword("1234");
        assertEquals("1234",userEntity.getPassword());
    }

    @Test
    void getCreatedAt() {
        userEntity.setCreatedAt(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))));
        assertEquals(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))),userEntity.getCreatedAt());
    }

    @Test
    void isDeleted() {
        userEntity.setDeleted(true);
        assertEquals(true,userEntity.isDeleted());
    }

    @Test
    void getRole() {
        userEntity.setRole(Role.USER);
        assertEquals(Role.USER,userEntity.getRole());
    }

    @Test
    void getCountry() {
        userEntity.setCountry(countryEntity);
        assertEquals(countryEntity,userEntity.getCountry());
    }

    @Test
    void testToString() {
        assertEquals("UserEntity(id=1, fullName=name, email=email@gmail.com, password=Nfoque@6530, createdAt="+LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")))+", isDeleted=false, role=USER, country=CountryEntity(id=null, code=null, name=null))",userEntity.toString());
    }
}