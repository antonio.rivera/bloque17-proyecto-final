package com.example.block17_proyecto_final.user.domain;

import com.example.block17_proyecto_final.country.domain.CountryEntity;
import com.example.block17_proyecto_final.country.infrastructure.dto.output.CountryOutputDto;
import com.example.block17_proyecto_final.user.domain.enums.Role;
import com.example.block17_proyecto_final.user.infrastructure.dto.input.UserInputDto;
import com.example.block17_proyecto_final.user.infrastructure.dto.input.UserSimpleInputDto;
import com.example.block17_proyecto_final.user.infrastructure.dto.output.UserOutputDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

class UserMapperTest {
//    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);
    private UserEntity userEntity;
    private UserInputDto userInputDto;
    private UserSimpleInputDto userSimpleInputDto;
    private UserOutputDto userOutputDto;
    private CountryEntity countryEntity;
    private CountryOutputDto countryOutputDto = new CountryOutputDto();
    @BeforeEach
    void setUp() {
        countryEntity= CountryEntity.builder()
                .id(1)
                .code("FR")
                .name("Francia")
                .build();
        countryOutputDto= CountryOutputDto.builder()
                .id(1)
                .code("FR")
                .name("Francia")
                .build();
        userEntity= UserEntity.builder()
                .id(1)
                .email("email@gmail.com")
                .role(Role.USER)
                .country(countryEntity)
                .password("Nfoque@6530")
                .fullName("name")
                .isDeleted(false)
                .createdAt(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))
                .build();
        userOutputDto= UserOutputDto.builder()
                .id(1)
                .email("email@gmail.com")
                .country(countryOutputDto)
                .password("Nfoque@6530")
                .fullName("name")
                .isDeleted(false)
                .createdAt(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))
                .build();
        userInputDto= userInputDto.builder()
                .email("email@gmail.com")
                .password("Nfoque@6530")
                .fullName("name")
                .country_id(1)
                .build();
        userSimpleInputDto= UserSimpleInputDto.builder()
                .id(1)
                .fullName("name")
                .build();

    }

    @Test
    void userInputToEntity() {
        assertEquals(userEntity.getEmail(),UserMapper.INSTANCE.userEntityToDto(userEntity).getEmail());
        assertEquals(userEntity.getPassword(),UserMapper.INSTANCE.userEntityToDto(userEntity).getPassword());
        assertEquals(userEntity.getFullName(),UserMapper.INSTANCE.userEntityToDto(userEntity).getFullName());
        assertEquals(userEntity.getCountry().getId(),UserMapper.INSTANCE.userEntityToDto(userEntity).getCountry().getId());

//        Simple inpùt Dto
        assertEquals(userEntity.getId(),userSimpleInputDto.getId());
        assertEquals(userEntity.getFullName(),userSimpleInputDto.getFullName());
    }

    @Test
    void userEntityToDto() {
        assertEquals(userOutputDto.getEmail(),UserMapper.INSTANCE.userInputToEntity(userInputDto).getEmail());
        assertEquals(userOutputDto.getPassword(),UserMapper.INSTANCE.userInputToEntity(userInputDto).getPassword());
        assertEquals(userOutputDto.getFullName(),UserMapper.INSTANCE.userInputToEntity(userInputDto).getFullName());
        assertEquals(userOutputDto.getCountry().getId(),UserMapper.INSTANCE.userInputToEntity(userInputDto).getCountry().getId());
    }
}