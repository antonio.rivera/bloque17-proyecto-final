package com.example.block17_proyecto_final.user.application.services;

import com.example.block17_proyecto_final.country.domain.CountryEntity;
import com.example.block17_proyecto_final.country.infrastructure.repository.CountryRepository;
import com.example.block17_proyecto_final.errors.exceptions.*;
import com.example.block17_proyecto_final.user.domain.UserEntity;
import com.example.block17_proyecto_final.user.domain.enums.Role;
import com.example.block17_proyecto_final.user.infrastructure.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


@DisplayName("Tests for User Service")
@ExtendWith(MockitoExtension.class)
class UserServiceTest {



    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;
    @Mock
    private CountryRepository countryRepository;

    @Mock
    private PasswordEncoder passwordEncoder;


    private UserEntity userEntity;
    private CountryEntity countryEntity;
    @BeforeEach
    void setUp() {
        userEntity= UserEntity.builder()
                .id(1)
                .email("email@gmail.com")
                .role(Role.USER)
                .country(new CountryEntity())
                .password("Nfoque@6530")
                .fullName("name")
                .isDeleted(false)
                .createdAt(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))
                .build();

        countryEntity= CountryEntity.builder()
                .id(1)
                .code("FR")
                .name("Francia")
                .build();
    }

    @DisplayName("Test to save user")
    @Test
    void saveUser() {
        Mockito.when(userRepository.save(userEntity)).thenReturn(userEntity);
        assertEquals(userEntity,userService.saveUser(userEntity));
        userEntity.setEmail("aaaa");
        assertThrows(EmailNotValidException.class,()->userService.saveUser(userEntity));
        userEntity.setEmail("email@gmail.com");
        userEntity.setPassword("aaaaa");
        assertThrows(InvalidPasswordException.class,()->userService.saveUser(userEntity));
    }
    @DisplayName("Test to save user with existing email")
    @Test
    void saveUserEmailExist() {
        Mockito.when(userRepository.existsByEmail("email@gmail.com")).thenReturn(true);
        assertThrows(EmailAlreadyExistsException.class,()->userService.saveUser(userEntity));
    }




    @DisplayName("Test to update user")
    @Test
    void modifyFullNameUser() {
        assertThrows(UserNotFoundException.class,()->userService.modifyFullNameUser(11,"aaa"));

        UserEntity userToModified= UserEntity.builder()
                .id(1)
                .email("email@gmail.com")
                .role(Role.USER)
                .country(new CountryEntity())
                .password("Nfoque@6530")
                .fullName("name2")
                .isDeleted(false)
                .createdAt(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))
                .build();
        Mockito.when(userRepository.findById(1)).thenReturn(Optional.ofNullable(userEntity));
        Mockito.when(userRepository.saveAndFlush(userToModified)).thenReturn(userToModified);
        assertEquals(userToModified.toString(),userService.modifyFullNameUser(1,"name2").toString());
        assertThrows(InvalidFullNameException.class,()->userService.modifyFullNameUser(1,""));

    }

    @DisplayName("Test to delete user")
    @Test
    void deleteUser() {
        UserEntity userToDeleted= UserEntity.builder()
                .id(1)
                .email("email@gmail.com")
                .role(Role.USER)
                .country(new CountryEntity())
                .password("Nfoque@6530")
                .fullName("name")
                .isDeleted(true)
                .createdAt(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))
                .build();
        Mockito.when(userRepository.findById(1)).thenReturn(Optional.ofNullable(userEntity));

        Mockito.when(userRepository.saveAndFlush(userToDeleted)).thenReturn(userToDeleted);
        assertEquals(userToDeleted,userService.deleteUser(1));
    }

    @DisplayName("Test to assign country to user")
    @Test
    void assignCountryToUser() {
        Mockito.when(userRepository.findById(1)).thenReturn(Optional.ofNullable(userEntity));
        Mockito.when(countryRepository.findById(1)).thenReturn(Optional.ofNullable(countryEntity));
        UserEntity userToModify= UserEntity.builder()
                .id(1)
                .email("email@gmail.com")
                .role(Role.USER)
                .country(countryEntity)
                .password("Nfoque@6530")
                .fullName("name")
                .isDeleted(false)
                .createdAt(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))
                .build();
        Mockito.when(userRepository.saveAndFlush(userToModify)).thenReturn(userToModify);
        assertEquals(userToModify,userService.assignCountryToUser(1,1));

    }

    @DisplayName("Test to read non deleted users")
    @Test
    void readNonDeletedUsers() {
        Mockito.when(userRepository.findAll()).thenReturn(List.of());
        assertEquals(List.of(),userService.readNonDeletedUsers());
    }




}