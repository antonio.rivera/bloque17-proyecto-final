package com.example.block17_proyecto_final.security.infrastructure.dto.output;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AuthResponseTest {
    private AuthResponse authResponse = new AuthResponse();

    @BeforeEach
    void setUp() {
        authResponse = AuthResponse.builder()
                .token("afsasf")
                .build();
    }

    @Test
    void getSetToken() {
        authResponse.setToken("aaa");
        assertEquals("aaa",authResponse.getToken());
    }

    @Test
    void testToString() {
        assertEquals("AuthResponse(token=afsasf)",authResponse.toString());
    }
}