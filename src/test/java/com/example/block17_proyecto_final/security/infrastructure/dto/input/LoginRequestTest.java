package com.example.block17_proyecto_final.security.infrastructure.dto.input;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LoginRequestTest {
    LoginRequest loginRequest = new LoginRequest();

    @BeforeEach
    void setUp() {
        loginRequest = LoginRequest.builder()
                .fullName("a")
                .password("Nfoque@6530")
                .build();
    }

    @Test
    void getSetFullName() {
        loginRequest.setFullName("e");
        assertEquals("e",loginRequest.getFullName());
    }

    @Test
    void getSetPassword() {
        loginRequest.setPassword("Nfoque@65301");
        assertEquals("Nfoque@65301",loginRequest.getPassword());
    }

    @Test
    void testToString() {
        assertEquals("LoginRequest(fullName=a, password=Nfoque@6530)",loginRequest.toString());
    }

}