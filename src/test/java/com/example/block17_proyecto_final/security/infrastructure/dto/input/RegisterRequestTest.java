package com.example.block17_proyecto_final.security.infrastructure.dto.input;

import com.example.block17_proyecto_final.user.domain.enums.Role;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RegisterRequestTest {
    private RegisterRequest registerRequest = new RegisterRequest();

    @BeforeEach
    void setUp() {
        registerRequest = RegisterRequest
                .builder()
                .email("email@gmail.com")
                .country_code("ae")
                .role(Role.USER)
                .fullName("full")
                .password("Nfoque@6530")
                .build();
    }

    @Test
    void getSetFullName() {
        registerRequest.setFullName("aaaa");
        assertEquals("aaaa",registerRequest.getFullName());
    }

    @Test
    void getEmail() {
        registerRequest.setEmail("aaaa@gmail.com");
        assertEquals("aaaa@gmail.com",registerRequest.getEmail());
    }

    @Test
    void getPassword() {
        registerRequest.setPassword("Nfoque@65301");
        assertEquals("Nfoque@65301",registerRequest.getPassword());
    }

    @Test
    void getCountry_code() {
        registerRequest.setCountry_code("ee");
        assertEquals("ee",registerRequest.getCountry_code());
    }

    @Test
    void getRole() {
        registerRequest.setRole(Role.ADMIN);
        assertEquals(Role.ADMIN,registerRequest.getRole());
    }

    @Test
    void testToString() {
        assertEquals("RegisterRequest(fullName=full, email=email@gmail.com, password=Nfoque@6530, country_code=ae, role=USER)",registerRequest.toString());
    }

}