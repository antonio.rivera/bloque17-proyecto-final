package com.example.block17_proyecto_final.order.domain;

import com.example.block17_proyecto_final.country.domain.CountryEntity;
import com.example.block17_proyecto_final.order.infrastructure.dto.input.OrderInputDto;
import com.example.block17_proyecto_final.order.infrastructure.dto.output.OrderOutputDto;
import com.example.block17_proyecto_final.user.domain.UserEntity;
import com.example.block17_proyecto_final.user.domain.UserMapper;
import com.example.block17_proyecto_final.user.domain.enums.Role;
import com.example.block17_proyecto_final.user.infrastructure.dto.output.UserOutputDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

class OrderMapperTest {

    private UserEntity userEntity;
    private CountryEntity countryEntity;

    private OrderEntity orderEntity;
    private OrderInputDto orderInputDto = new OrderInputDto();
    private OrderOutputDto orderOutputDto = new OrderOutputDto();
    @BeforeEach
    void setUp() {
        countryEntity= CountryEntity.builder()
                .id(1)
                .code("FR")
                .name("Francia")
                .build();
        userEntity= UserEntity.builder()
                .id(1)
                .email("email@gmail.com")
                .role(Role.USER)
                .country(countryEntity)
                .password("Nfoque@6530")
                .fullName("name")
                .isDeleted(false)
                .createdAt(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))
                .build();
        orderEntity = OrderEntity.builder()
                .id(1)
                .status("status")
                .createdAt(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))
                .user(userEntity)
                .build();
        orderInputDto = OrderInputDto.builder()
                .status("status")
                .createdAt(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))
                .user_id(1)
                .build();

        orderOutputDto = OrderOutputDto.builder()
                .status("status")
                .createdAt(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))
                .user(UserMapper.INSTANCE.userEntityToDto(userEntity))
                .build();
    }

    @Test
    void orderInputToEntity() {
        assertEquals(orderEntity.getId(),OrderMapper.INSTANCE.orderEntityToDto(orderEntity).getId());
        assertEquals(orderEntity.getStatus(),OrderMapper.INSTANCE.orderEntityToDto(orderEntity).getStatus());
        assertEquals(orderEntity.getCreatedAt(),OrderMapper.INSTANCE.orderEntityToDto(orderEntity).getCreatedAt());
        assertEquals(orderEntity.getUser().getId(),OrderMapper.INSTANCE.orderEntityToDto(orderEntity).getUser().getId());
    }

    @Test
    void orderEntityToDto() {
        assertEquals(orderOutputDto.getStatus(),OrderMapper.INSTANCE.orderInputToEntity(orderInputDto).getStatus());
        assertEquals(orderOutputDto.getCreatedAt(),OrderMapper.INSTANCE.orderInputToEntity(orderInputDto).getCreatedAt());
        assertEquals(orderOutputDto.getUser().getId(),OrderMapper.INSTANCE.orderInputToEntity(orderInputDto).getUser().getId());
    }
}