package com.example.block17_proyecto_final.order.domain;

import com.example.block17_proyecto_final.user.domain.UserEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

class OrderEntityTest {

    private OrderEntity orderEntity=new OrderEntity();
    private OrderEntity orderEntity2=new OrderEntity();
    private LocalDate createdAt;

    @BeforeEach
    void setUp() {
        createdAt = LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        orderEntity = OrderEntity.builder()
                .id(1)
                .status("status")
                .createdAt(createdAt)
                .user(new UserEntity())
                .build();
        orderEntity2 = OrderEntity.builder()
                .id(1)
                .status("status")
                .createdAt(createdAt)
                .user(new UserEntity())
                .build();
    }

    @Test
    void testEquals() {
        assertEquals(true, orderEntity2.equals(orderEntity));
    }

    @Test
    void canEqual() {
        assertEquals(true, orderEntity2.canEqual(orderEntity));
    }

    @Test
    void testHashCode() {
        assertEquals(orderEntity.hashCode(), orderEntity2.hashCode());
    }

    @Test
    void getSetId() {
        orderEntity.setId(2);
        assertEquals(2, orderEntity.getId());
    }

    @Test
    void getSetStatus() {
        orderEntity.setStatus("qq");
        assertEquals("qq", orderEntity.getStatus());
    }

    @Test
    void getSetCreatedAt() {
        orderEntity.setCreatedAt(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))));
        assertEquals(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))), orderEntity2.getCreatedAt());
    }

    @Test
    void getSetUser() {
        orderEntity.setUser(new UserEntity());
        assertEquals(new UserEntity(), orderEntity.getUser());
    }

    @Test
    void testToString() {
        assertEquals("OrderEntity(id=1, status=status, createdAt="+createdAt+", user=UserEntity(id=null, fullName=null, email=null, password=null, createdAt=null, isDeleted=false, role=null, country=null))", orderEntity.toString());
    }
}