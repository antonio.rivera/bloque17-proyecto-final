package com.example.block17_proyecto_final.order.application.services;

import com.example.block17_proyecto_final.country.application.services.CountryServiceImpl;
import com.example.block17_proyecto_final.country.infrastructure.repository.CountryRepository;
import com.example.block17_proyecto_final.order.domain.OrderEntity;
import com.example.block17_proyecto_final.order.infrastructure.repository.OrderRepository;
import com.example.block17_proyecto_final.user.domain.UserEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
@DisplayName("Tests for Order Service")
@ExtendWith(MockitoExtension.class)
class OrderServiceTest {
    @InjectMocks
    private OrderServiceImpl orderService;

    @Mock
    private OrderRepository orderRepository;

    private OrderEntity orderEntity;
    private LocalDate createdAt;


    @BeforeEach
    void setUp() {
        createdAt = LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));


        orderEntity = OrderEntity.builder()
                .id(1)
                .status("status")
                .createdAt(createdAt)
                .user(new UserEntity())
                .build();
    }

    @DisplayName("Test to save order")
    @Test
    void saveOrder() {
        Mockito.when(orderRepository.save(orderEntity)).thenReturn(orderEntity);
        assertEquals(orderEntity,orderService.saveOrder(orderEntity));
    }
}