package com.example.block17_proyecto_final.country.application.services;

import com.example.block17_proyecto_final.country.domain.CountryEntity;
import com.example.block17_proyecto_final.country.infrastructure.repository.CountryRepository;
import com.example.block17_proyecto_final.errors.exceptions.CountryNotFoundException;
import com.example.block17_proyecto_final.errors.exceptions.InvalidCountryToDeleteException;
import com.example.block17_proyecto_final.errors.exceptions.InvalidPasswordException;
import com.example.block17_proyecto_final.user.domain.UserEntity;
import com.example.block17_proyecto_final.user.domain.enums.Role;
import com.example.block17_proyecto_final.user.infrastructure.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Tests for Country Service")
@ExtendWith(MockitoExtension.class)
class CountryServiceTest {
    @InjectMocks
    private CountryServiceImpl countryService;

    @Mock
    private CountryRepository countryRepository;

    @Mock
    private UserRepository userRepository;

    private CountryEntity countryEntity;

    private UserEntity userEntity;
    @BeforeEach
    void setUp() {
        countryEntity= CountryEntity.builder()
                .id(1)
                .code("FR")
                .name("Francia")
                .build();
        userEntity= UserEntity.builder()
                .id(1)
                .email("email@gmail.com")
                .role(Role.USER)
                .country(countryEntity)
                .password("Nfoque@6530")
                .fullName("name")
                .isDeleted(false)
                .createdAt(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))
                .build();
    }

    @DisplayName("Test to save country")
    @Test
    void saveCountry() {
        Mockito.when(countryRepository.save(countryEntity)).thenReturn(countryEntity);
        assertEquals(countryEntity,countryService.saveCountry(countryEntity));
    }

    @DisplayName("Test to update country")
    @Test
    void updateCountry() {
        CountryEntity countryToModified= CountryEntity.builder()
                .id(1)
                .code("PH")
                .name("Filipinas")
                .build();
        Mockito.when(countryRepository.findById(1)).thenReturn(Optional.ofNullable(countryEntity));
        Mockito.when(countryRepository.saveAndFlush(countryToModified)).thenReturn(countryToModified);
        assertEquals(countryToModified,countryService.updateCountry(1,countryToModified));
    }

    @DisplayName("Test to delete country")
    @Test
    void deleteCountry() {
        Mockito.when(countryRepository.findById(1)).thenReturn(Optional.ofNullable(countryEntity));
        assertEquals(countryEntity,countryService.deleteCountry(1));

    }
    @DisplayName("Test to delete country that not exists")
    @Test
    void deleteCountryFail() {
        //Mockito.when(countryRepository.findById(1)).thenReturn(Optional.ofNullable(countryEntity));
//        assertEquals(countryEntity,countryService.deleteCountry(100));
        assertThrows(CountryNotFoundException.class,()->countryService.deleteCountry(100));
    }
    @DisplayName("Test to delete country that are assign to user")
    @Test
    void deleteCountryFail2() {

        Mockito.when(userRepository.findAll().stream().filter(userEntity -> !userEntity.isDeleted()).filter(userEntity -> userEntity.getCountry().getId().equals(1)).collect(Collectors.toList())).thenReturn(List.of(userEntity));
        assertThrows(InvalidCountryToDeleteException.class,()->countryService.deleteCountry(1));
    }

    @DisplayName("Test to read all countries")
    @Test
    void readAllCountries() {
        Mockito.when(countryRepository.findAll()).thenReturn(List.of());
        assertEquals(List.of(),countryService.readAllCountries());
    }
}