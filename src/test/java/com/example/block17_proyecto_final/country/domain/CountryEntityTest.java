package com.example.block17_proyecto_final.country.domain;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
@DisplayName("Tests for Country Entity")
@ExtendWith(MockitoExtension.class)
class CountryEntityTest {

    @Test
    void getSetId() {
        CountryEntity country = new CountryEntity();
        country.setId(1);
        assertEquals(1,country.getId());
    }

    @Test
    void getSetCode() {
        CountryEntity country = new CountryEntity();
        country.setCode("ae");
        assertEquals("ae",country.getCode());
    }

    @Test
    void getSetName() {
        CountryEntity country = new CountryEntity();
        country.setName("ae");
        assertEquals("ae",country.getName());
    }

    @Test
    void testEquals() {
        CountryEntity country = new CountryEntity();
        assertEquals(true,new CountryEntity().equals(country));
    }

    @Test
    void testHashCode() {
        CountryEntity country = new CountryEntity();
        assertEquals(new CountryEntity().hashCode(),country.hashCode());
    }

    @Test
    void testToString() {
        CountryEntity country = new CountryEntity();
        assertEquals("CountryEntity(id=null, code=null, name=null)",country.toString());
    }
}