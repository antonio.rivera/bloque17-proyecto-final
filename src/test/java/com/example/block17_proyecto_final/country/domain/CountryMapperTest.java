package com.example.block17_proyecto_final.country.domain;

import com.example.block17_proyecto_final.country.infrastructure.dto.input.CountryInputDto;
import com.example.block17_proyecto_final.country.infrastructure.dto.output.CountryOutputDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CountryMapperTest {
    private CountryEntity countryEntity;
    private CountryOutputDto countryOutputDto = new CountryOutputDto();
    private CountryInputDto countryInputDto = new CountryInputDto();
    @BeforeEach
    void setUp() {
        countryEntity= CountryEntity.builder()
                .id(1)
                .code("FR")
                .name("Francia")
                .build();
        countryInputDto= CountryInputDto.builder()
                .code("FR")
                .name("Francia")
                .build();
        countryOutputDto= CountryOutputDto.builder()
                .id(1)
                .code("FR")
                .name("Francia")
                .build();
    }

    @Test
    void countryInputToEntity() {
        assertEquals(countryEntity.getCode(),CountryMapper.INSTANCE.countryInputToEntity(countryInputDto).getCode());
        assertEquals(countryEntity.getName(),CountryMapper.INSTANCE.countryInputToEntity(countryInputDto).getName());
    }

    @Test
    void countryEntityToDto() {
        assertEquals(countryOutputDto.getId(),CountryMapper.INSTANCE.countryEntityToDto(countryEntity).getId());
        assertEquals(countryOutputDto.getCode(),CountryMapper.INSTANCE.countryEntityToDto(countryEntity).getCode());
        assertEquals(countryOutputDto.getName(),CountryMapper.INSTANCE.countryEntityToDto(countryEntity).getName());
    }
}