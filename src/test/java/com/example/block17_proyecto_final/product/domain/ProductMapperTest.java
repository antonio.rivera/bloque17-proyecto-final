package com.example.block17_proyecto_final.product.domain;

import com.example.block17_proyecto_final.product.infrastructure.dto.input.ProductInputDto;
import com.example.block17_proyecto_final.product.infrastructure.dto.output.ProductOutputDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

class ProductMapperTest {
    private ProductEntity productEntity;
    private ProductInputDto productInputDto = new ProductInputDto();
    private ProductOutputDto productOutputDto = new ProductOutputDto();

    @BeforeEach
    void setUp() {
        productEntity= ProductEntity.builder()
                .id(1)
                .status("status")
                .price(10F)
                .createdAt(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))
                .name("name")
                .build();
        productInputDto= ProductInputDto.builder()
                .status("status")
                .price(10F)
                .createdAt(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))
                .name("name")
                .build();
        productOutputDto = ProductOutputDto.builder()
                .id(1)
                .status("status")
                .price(10F)
                .createdAt(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))
                .name("name")
                .build();

    }

    @Test
    void productInputToEntity() {
        assertEquals(productEntity.getName(),ProductMapper.INSTANCE.productInputToEntity(productInputDto).getName());
        assertEquals(productEntity.getPrice(),ProductMapper.INSTANCE.productInputToEntity(productInputDto).getPrice());
        assertEquals(productEntity.getStatus(),ProductMapper.INSTANCE.productInputToEntity(productInputDto).getStatus());
        assertEquals(productEntity.getCreatedAt(),ProductMapper.INSTANCE.productInputToEntity(productInputDto).getCreatedAt());
    }

    @Test
    void productEntityToDto() {
        assertEquals(productOutputDto.getName(),ProductMapper.INSTANCE.productEntityToDto(productEntity).getName());
        assertEquals(productOutputDto.getPrice(),ProductMapper.INSTANCE.productEntityToDto(productEntity).getPrice());
        assertEquals(productOutputDto.getStatus(),ProductMapper.INSTANCE.productEntityToDto(productEntity).getStatus());
        assertEquals(productOutputDto.getCreatedAt(),ProductMapper.INSTANCE.productEntityToDto(productEntity).getCreatedAt());
        assertEquals(productOutputDto.getId(),ProductMapper.INSTANCE.productEntityToDto(productEntity).getId());
    }
}