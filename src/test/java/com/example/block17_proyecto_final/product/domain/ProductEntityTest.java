package com.example.block17_proyecto_final.product.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

class ProductEntityTest {

    private ProductEntity productEntity = new ProductEntity();
    private ProductEntity productEntity2 = new ProductEntity();
    private LocalDate createdAt;


    @BeforeEach
    void setUp() {
        createdAt = LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));

        productEntity = ProductEntity.builder()
                .id(1)
                .price(10F)
                .name("aaa")
                .status("qqqq")
                .createdAt(createdAt)
                .build();
        productEntity2 = ProductEntity.builder()
                .id(1)
                .price(10F)
                .name("aaa")
                .status("qqqq")
                .createdAt(createdAt)
                .build();
    }

    @Test
    void testEquals() {
        assertEquals(true,productEntity.equals(productEntity2));
        assertEquals(true,productEntity.getName().equals(productEntity2.getName()));
        assertEquals(true,productEntity.getStatus().equals(productEntity2.getStatus()));
    }

    @Test
    void canEqual() {
        assertEquals(true,productEntity.canEqual(productEntity2));

    }

    @Test
    void testHashCode() {
        assertEquals(productEntity.hashCode(),productEntity2.hashCode());
    }

    @Test
    void getId() {
        productEntity.setId(1);
        assertEquals(1,productEntity.getId());
    }

    @Test
    void getName() {
        productEntity.setName("ttt");
        assertEquals("ttt",productEntity.getName());
    }

    @Test
    void getPrice() {
        productEntity.setPrice(1F);
        assertEquals(1F,productEntity.getPrice());
    }

    @Test
    void getStatus() {
        productEntity.setStatus("rrr");
        assertEquals("rrr",productEntity.getStatus());
    }

    @Test
    void getCreatedAt() {
        productEntity.setCreatedAt(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))));
        assertEquals(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))),productEntity.getCreatedAt());
    }

    @Test
    void testToString() {
        assertEquals("ProductEntity(id=1, name=aaa, price=10.0, status=qqqq, createdAt="+createdAt+")",productEntity.toString());
    }
}