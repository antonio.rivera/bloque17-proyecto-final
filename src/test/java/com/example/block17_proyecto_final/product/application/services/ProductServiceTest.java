package com.example.block17_proyecto_final.product.application.services;

import com.example.block17_proyecto_final.errors.exceptions.ProductNotFoundException;
import com.example.block17_proyecto_final.product.domain.ProductEntity;
import com.example.block17_proyecto_final.product.infrastructure.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
@DisplayName("Tests for Product Service")
@ExtendWith(MockitoExtension.class)
class ProductServiceTest {
    @InjectMocks
    private ProductServiceImpl productService;

    @Mock
    private ProductRepository productRepository;




    private ProductEntity productEntity;
    @BeforeEach
    void setUp() {
        productEntity= ProductEntity.builder()
                .id(1)
                .status("status")
                .price(10F)
                .createdAt(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))
                .name("name")
                .build();
    }

    @DisplayName("Test to save product")
    @Test
    void saveProduct() {
        Mockito.when(productRepository.save(productEntity)).thenReturn(productEntity);
        assertEquals(productEntity,productService.saveProduct(productEntity));
    }


    @DisplayName("Test to update product")
    @Test
    void updateProduct() {
        ProductEntity productToModified= ProductEntity.builder()
                .id(1)
                .status("status2")
                .price(10F)
                .createdAt(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))
                .name("name2")
                .build();
        Mockito.when(productRepository.findById(1)).thenReturn(Optional.ofNullable(productEntity));
        Mockito.when(productRepository.saveAndFlush(productToModified)).thenReturn(productToModified);
        assertEquals(productToModified,productService.updateProduct(1,productToModified));
    }

    @DisplayName("Test to delete product")
    @Test
    void deleteProduct() {
        assertThrows(ProductNotFoundException.class,()->productService.deleteProduct(10));
        Mockito.when(productRepository.findById(1)).thenReturn(Optional.ofNullable(productEntity));
        assertEquals(productEntity,productService.deleteProduct(1));
    }

    @DisplayName("Test to read all products")
    @Test
    void readAllCountries() {
        Mockito.when(productRepository.findAll()).thenReturn(List.of());
        assertEquals(List.of(),productService.readAllProducts());
    }


}