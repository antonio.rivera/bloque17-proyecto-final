package com.example.block17_proyecto_final.product.config;

import com.example.block17_proyecto_final.product.domain.ProductEntity;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public class ProductConfig implements Specification<ProductEntity> {
    private boolean name;

    private boolean price;

    private boolean status;

    private boolean createdAt;

    @Override
    public Predicate toPredicate(Root<ProductEntity> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicateList = new ArrayList<>();
        if (name){
            query.orderBy(criteriaBuilder.asc(root.get("name")));
        }
        if (price){
            query.orderBy(criteriaBuilder.asc(root.get("price")));
        }
        if (status){
            query.orderBy(criteriaBuilder.asc(root.get("status")));
        }
        if (createdAt){
            query.orderBy(criteriaBuilder.asc(root.get("createdAt")));
        }
        return criteriaBuilder.and(predicateList.toArray(new Predicate[predicateList.size()]));
    }
}
