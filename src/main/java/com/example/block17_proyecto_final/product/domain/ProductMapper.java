package com.example.block17_proyecto_final.product.domain;
import com.example.block17_proyecto_final.product.domain.ProductEntity;
import com.example.block17_proyecto_final.product.infrastructure.dto.input.ProductInputDto;
import com.example.block17_proyecto_final.product.infrastructure.dto.output.ProductOutputDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;


@Mapper(componentModel = "spring")
public interface ProductMapper {
    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);


    ProductEntity productInputToEntity(ProductInputDto productInputDto);

    ProductOutputDto productEntityToDto(ProductEntity productEntity);

}
