package com.example.block17_proyecto_final.product.infrastructure.controller;


import com.example.block17_proyecto_final.product.application.services.ProductService;
import com.example.block17_proyecto_final.product.config.ProductConfig;
import com.example.block17_proyecto_final.product.domain.ProductEntity;
import com.example.block17_proyecto_final.product.domain.ProductMapper;
import com.example.block17_proyecto_final.product.infrastructure.dto.input.ProductInputDto;
import com.example.block17_proyecto_final.product.infrastructure.dto.output.ProductOutputDto;
import com.example.block17_proyecto_final.product.infrastructure.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/product")
public class ProductController {
    private final ProductService productService;
    private final ProductRepository productRepository;

    /**
     * Endopoint where we create new product
     * Only ADMIN can access
     * @param productInputDto
     * @return ResponseEntity
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping()
    public ResponseEntity<ProductOutputDto> createProduct(@RequestBody ProductInputDto productInputDto){
        ProductEntity productEntity = ProductMapper.INSTANCE.productInputToEntity(productInputDto);
        ProductOutputDto productOutputDto = ProductMapper.INSTANCE.productEntityToDto(productService.saveProduct(productEntity));
        return new ResponseEntity<>(productOutputDto, HttpStatus.OK);
    }

    /**
     * Endopoint where we update one product by id
     * Only ADMIN can access
     * @param id
     * @param productInputDto
     * @return ResponseEntity
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    @PutMapping("{id}")
    public ResponseEntity<ProductOutputDto> updateProduct(@PathVariable Integer id, @RequestBody ProductInputDto productInputDto){
        ProductEntity productToModify = productService.updateProduct(id,ProductMapper.INSTANCE.productInputToEntity(productInputDto));
        ProductOutputDto productOutputDto = ProductMapper.INSTANCE.productEntityToDto(productToModify);
        return new ResponseEntity<>(productOutputDto, HttpStatus.OK);
    }


    /**
     * Endopoint where we delete one product by id
     * Only ADMIN can access
     * @param id
     * @return ResponseEntity
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping("{id}")
    public ResponseEntity<ProductOutputDto> deleteProduct(@PathVariable Integer id){
        ProductEntity productTodelete = productService.deleteProduct(id);
        ProductOutputDto productOutputDto = ProductMapper.INSTANCE.productEntityToDto(productTodelete);
        return new ResponseEntity<>(productOutputDto, HttpStatus.OK);
    }


    /**
     * Endpoint where we can find all products and apply criteria filter
     * ADMIN and USER can access
     * @param name
     * @param price
     * @param status
     * @param createdAt
     * @return ResponseEntity
     */
    @PreAuthorize("hasAuthority('ADMIN')||hasAuthority('USER')")
    @GetMapping("/criteria")
    public ResponseEntity<List<ProductEntity>> showAllProducts(@RequestParam(required = false,defaultValue = "false") boolean name,
                                                               @RequestParam(required = false,defaultValue = "false") boolean price,
                                                               @RequestParam(required = false,defaultValue = "false") boolean status,
                                                               @RequestParam(required = false,defaultValue = "false") boolean createdAt){
        ProductConfig searchProductSpecification = new ProductConfig(name, price, status, createdAt);
        return new ResponseEntity<>(productRepository.findAll(searchProductSpecification),HttpStatus.OK);
    }




}
