package com.example.block17_proyecto_final.product.infrastructure.dto.output;

import lombok.*;

import java.time.LocalDate;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductOutputDto {
    private Integer id;

    private String name;

    private Float price;

    private String status;

    private LocalDate createdAt;
}
