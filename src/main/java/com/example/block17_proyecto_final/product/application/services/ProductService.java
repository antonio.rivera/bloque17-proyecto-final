package com.example.block17_proyecto_final.product.application.services;

import com.example.block17_proyecto_final.product.domain.ProductEntity;

import java.util.List;

public interface ProductService {
    ProductEntity saveProduct(ProductEntity product);

    ProductEntity updateProduct(Integer id, ProductEntity product);

    ProductEntity deleteProduct(Integer id);

    List<ProductEntity> readAllProducts();
}
