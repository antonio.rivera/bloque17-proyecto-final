package com.example.block17_proyecto_final.product.application.services;

import com.example.block17_proyecto_final.errors.exceptions.ProductNotFoundException;
import com.example.block17_proyecto_final.product.domain.ProductEntity;
import com.example.block17_proyecto_final.product.infrastructure.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService{
    private final ProductRepository productRepository;
    @Override
    public ProductEntity saveProduct(ProductEntity product) {
        product.setCreatedAt(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))));
        return productRepository.save(product);
    }

    @Override
    public ProductEntity updateProduct(Integer id, ProductEntity product) {
        ProductEntity productToModify = productRepository.findById(id).orElseThrow(()->new ProductNotFoundException("Product not exists in database"));

        productToModify.setName(product.getName());
        productToModify.setPrice(product.getPrice());
        productToModify.setStatus(product.getStatus());
        return productRepository.saveAndFlush(productToModify);
    }

    @Override
    public ProductEntity deleteProduct(Integer id) {
        ProductEntity productToDelete = productRepository.findById(id).orElseThrow(()->new ProductNotFoundException("Product not exists in database"));
        productRepository.deleteById(id);
        return productToDelete;
    }

    @Override
    public List<ProductEntity> readAllProducts() {
        return productRepository.findAll();
    }
}
