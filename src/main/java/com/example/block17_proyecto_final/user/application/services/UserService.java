package com.example.block17_proyecto_final.user.application.services;

import com.example.block17_proyecto_final.user.domain.UserEntity;

import java.util.List;

public interface UserService {
    List<UserEntity> readNonDeletedUsers();

    UserEntity saveUser(UserEntity user);

    UserEntity modifyFullNameUser(Integer id, String fullName);

    UserEntity deleteUser(Integer id);

    UserEntity assignCountryToUser(Integer idCountry, Integer idUser);

}
