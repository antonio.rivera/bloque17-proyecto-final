package com.example.block17_proyecto_final.user.application.services;


import com.example.block17_proyecto_final.country.domain.CountryEntity;
import com.example.block17_proyecto_final.country.infrastructure.repository.CountryRepository;
import com.example.block17_proyecto_final.errors.exceptions.*;
import com.example.block17_proyecto_final.user.domain.UserEntity;
import com.example.block17_proyecto_final.user.domain.enums.Role;
import com.example.block17_proyecto_final.user.infrastructure.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService{
    private final UserRepository userRepository;
    private final CountryRepository countryRepository;
    private static final Pattern EMAIL_PATTERN = Pattern.compile("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$");
    private static final Pattern PASSWORD_PATTERN =Pattern.compile("^(?=.*[A-Z])(?=.*[a-z])(?=.*[\\d])(?=.*[~`!@#$%^&*()\\-_=+\\[{\\]}|:;\"'<,>.?/]).{8,}$");

    private final PasswordEncoder passwordEncoder;
    @Override
    public List<UserEntity> readNonDeletedUsers() {
        return userRepository.findAll()
                .stream()
                .filter(userEntity -> !userEntity.isDeleted())
                .collect(Collectors.toList());
    }

    @Override
    public UserEntity saveUser(UserEntity user) {
        if (userRepository.existsByEmail(user.getEmail())){
            throw new EmailAlreadyExistsException("Email already exists");
        }
        if (!EMAIL_PATTERN.matcher(user.getEmail()).matches()){
            throw new EmailNotValidException("Not valid email");
        }
        if (!PASSWORD_PATTERN.matcher(user.getPassword()).matches()){
            throw new InvalidPasswordException("Not valid password");
        }
        user.setCreatedAt(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))));
        user.setRole(Role.USER);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    @Override
    public UserEntity modifyFullNameUser(Integer id, String fullName) {
        if (fullName.isEmpty()){
            throw new InvalidFullNameException("Full name can not be empty");
        }
        UserEntity userToModify = userRepository.findById(id).orElseThrow(()-> new UserNotFoundException("User not found in database"));
        userToModify.setFullName(fullName);
        return userRepository.saveAndFlush(userToModify);
    }

    @Override
    public UserEntity deleteUser(Integer id) {
        UserEntity userToDelete = userRepository.findById(id).orElseThrow(()-> new UserNotFoundException("User not found in database"));
        userToDelete.setDeleted(true);
        return userRepository.saveAndFlush(userToDelete);
    }

    @Override
    public UserEntity assignCountryToUser(Integer idCountry, Integer idUser) {
        UserEntity userToModify = userRepository.findById(idUser).orElseThrow(()-> new UserNotFoundException("User not found in database"));
            CountryEntity country = countryRepository.findById(idCountry).orElseThrow(()-> new CountryNotFoundException("Country not found in database"));
            userToModify.setCountry(country);


        return userRepository.saveAndFlush(userToModify);
    }


}
