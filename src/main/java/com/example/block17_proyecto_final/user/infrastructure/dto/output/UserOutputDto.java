package com.example.block17_proyecto_final.user.infrastructure.dto.output;

import com.example.block17_proyecto_final.country.infrastructure.dto.output.CountryOutputDto;
import lombok.*;

import java.time.LocalDate;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserOutputDto {
    private Integer id;

    private String fullName;

    private String email;

    private String password;

    private LocalDate createdAt;

    private boolean isDeleted;

    private CountryOutputDto country;
}
