package com.example.block17_proyecto_final.user.infrastructure.dto.input;


import lombok.*;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserSimpleInputDto {
    private Integer id;

    private String fullName;
}
