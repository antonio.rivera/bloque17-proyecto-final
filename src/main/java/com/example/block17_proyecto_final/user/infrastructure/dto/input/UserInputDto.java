package com.example.block17_proyecto_final.user.infrastructure.dto.input;


import lombok.*;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserInputDto {
    private String fullName;

    private String email;

    private String password;

    private Integer country_id;
}
