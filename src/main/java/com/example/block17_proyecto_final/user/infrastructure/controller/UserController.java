package com.example.block17_proyecto_final.user.infrastructure.controller;



import com.example.block17_proyecto_final.country.domain.CountryEntity;
import com.example.block17_proyecto_final.country.infrastructure.dto.input.CountryInputDto;
import com.example.block17_proyecto_final.country.infrastructure.repository.CountryRepository;
import com.example.block17_proyecto_final.user.application.services.UserService;
import com.example.block17_proyecto_final.user.domain.UserEntity;
import com.example.block17_proyecto_final.user.domain.UserMapper;
import com.example.block17_proyecto_final.user.domain.enums.Role;
import com.example.block17_proyecto_final.user.infrastructure.dto.input.UserInputDto;
import com.example.block17_proyecto_final.user.infrastructure.dto.input.UserSimpleInputDto;
import com.example.block17_proyecto_final.user.infrastructure.dto.output.UserOutputDto;
import com.example.block17_proyecto_final.user.infrastructure.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    private final UserRepository userRepository;

    private final CountryRepository countryRepository;


    /**
     * Endpoint where we can show all users that are not deleted
     * Only ADMIN can access
     * @return ResponseEntity
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping()
    public ResponseEntity<List<UserOutputDto>> findAllUsers(){
        List<UserOutputDto> usersOutput =
                userService.readNonDeletedUsers()
                .stream()
                .map(user -> UserMapper.INSTANCE.userEntityToDto(user))
                .collect(Collectors.toList());
        return new ResponseEntity<>(usersOutput, HttpStatus.OK);
    }

    /**
     * Endpoint where we create user
     * Only ADMIN can access
     * @param userInputDto
     * @return ResponseEntity
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping()
    public ResponseEntity<UserOutputDto> saveUser(@RequestBody UserInputDto userInputDto){
        UserEntity userEntityInput = UserMapper.INSTANCE.userInputToEntity(userInputDto);
        userEntityInput.setCountry(countryRepository.findById(userInputDto.getCountry_id()).get());
        UserOutputDto userOutputDto = UserMapper.INSTANCE.userEntityToDto(userService.saveUser(userEntityInput));
        return new ResponseEntity<>(userOutputDto,HttpStatus.OK);
    }

    /**
     * Endpoint where we can modify full name user
     * Only ADMIN can access
     * @param userSimpleInputDto
     * @return ResponseEntity
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    @PutMapping()
    public ResponseEntity<UserOutputDto> modifyFullNameUser(@RequestBody UserSimpleInputDto userSimpleInputDto){
        UserEntity user = userService.modifyFullNameUser(userSimpleInputDto.getId(),userSimpleInputDto.getFullName());
        UserOutputDto userOutputDto=
                UserMapper.INSTANCE.userEntityToDto(user);
        return new ResponseEntity<>(userOutputDto,HttpStatus.OK);
    }

    /**
     * Endpoint where we can delete user by id
     * Only ADMIN can access
     * @param id
     * @return ResponseEntity
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<UserOutputDto> deleteUser(@PathVariable Integer id){
        UserOutputDto userOutputDto=
                UserMapper.INSTANCE.userEntityToDto(userService.deleteUser(id));
        return new ResponseEntity<>(userOutputDto,HttpStatus.OK);
    }


    /**
     * Endpoint where we can assign country to user
     * ADMIN and USER can access
     * @param idCountry
     * @param idUser
     * @return ResponseEntity
     */
    @PreAuthorize("hasAuthority('ADMIN')||hasAuthority('USER')")
    @PutMapping("/assignCountryToUser")
    public ResponseEntity<UserOutputDto> assignCountryToUser(@RequestParam Integer idCountry, @RequestParam Integer idUser){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        UserEntity userModified = new UserEntity();
        if (userRepository.findByFullName(userDetails.getUsername()).get().getRole().equals(Role.ADMIN)){
            userModified=userService.assignCountryToUser(idCountry, idUser);
        }
        if (userRepository.findByFullName(userDetails.getUsername()).get().getRole().equals(Role.USER)){
            userModified=userService.assignCountryToUser(idCountry, userRepository.findByFullName(userDetails.getUsername()).get().getId());
        }
        return new ResponseEntity<>(UserMapper.INSTANCE.userEntityToDto(userModified),HttpStatus.OK );

    }


}
