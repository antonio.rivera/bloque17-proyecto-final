package com.example.block17_proyecto_final.user.infrastructure.repository;

import com.example.block17_proyecto_final.user.domain.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity, Integer> {
    boolean existsByEmail(String email);

    Optional<UserEntity> findByFullName(String fullName);
}
