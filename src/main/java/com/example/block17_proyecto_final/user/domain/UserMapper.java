package com.example.block17_proyecto_final.user.domain;
import com.example.block17_proyecto_final.user.infrastructure.dto.input.UserInputDto;
import com.example.block17_proyecto_final.user.infrastructure.dto.output.UserOutputDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;


@Mapper(componentModel = "spring")
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);


    @Mapping(source = "country_id", target = "country.id")
    UserEntity userInputToEntity(UserInputDto userInputDto);



    UserOutputDto userEntityToDto(UserEntity userEntity);

}
