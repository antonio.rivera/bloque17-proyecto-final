package com.example.block17_proyecto_final.user.domain.enums;

public enum Role {
    ADMIN,
    USER
}
