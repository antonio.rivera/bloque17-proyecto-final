package com.example.block17_proyecto_final;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Block17ProyectoFinalApplication {

	public static void main(String[] args) {
		SpringApplication.run(Block17ProyectoFinalApplication.class, args);
	}

}
