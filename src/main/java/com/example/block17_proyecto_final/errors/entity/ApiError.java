package com.example.block17_proyecto_final.errors.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;
@Getter
@Setter
public class ApiError {
    private HttpStatus status;
    private String message;
    private Timestamp timestamp = new Timestamp(System.currentTimeMillis());

    public ApiError(HttpStatus httpStatus, String message) {
        this.status = httpStatus;
        this.message = message;
    }
}
