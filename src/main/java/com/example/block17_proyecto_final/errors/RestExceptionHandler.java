package com.example.block17_proyecto_final.errors;


import com.example.block17_proyecto_final.errors.entity.ApiError;
import com.example.block17_proyecto_final.errors.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler {
    @ExceptionHandler(EmailNotValidException.class)
    public ResponseEntity<ApiError> handleEmailNotValidException(EmailNotValidException e){
        ApiError apiError = new ApiError(HttpStatus.UNPROCESSABLE_ENTITY,e.getMessage());
        return new ResponseEntity<>(apiError,apiError.getStatus());
    }
    @ExceptionHandler(EmailAlreadyExistsException.class)
    public ResponseEntity<ApiError> handleEmailAlreadyExistsException(EmailAlreadyExistsException e){
        ApiError apiError = new ApiError(HttpStatus.CONFLICT,e.getMessage());
        return new ResponseEntity<>(apiError,apiError.getStatus());
    }
    @ExceptionHandler(InvalidPasswordException.class)
    public ResponseEntity<ApiError> handleInvalidPasswordException(InvalidPasswordException e){
        ApiError apiError = new ApiError(HttpStatus.UNPROCESSABLE_ENTITY,e.getMessage());
        return new ResponseEntity<>(apiError,apiError.getStatus());
    }
    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<ApiError> handleUserNotFoundException(UserNotFoundException e){
        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND,e.getMessage());
        return new ResponseEntity<>(apiError,apiError.getStatus());
    }
    @ExceptionHandler(InvalidFullNameException.class)
    public ResponseEntity<ApiError> handleInvalidFullNameException(InvalidFullNameException e){
        ApiError apiError = new ApiError(HttpStatus.UNPROCESSABLE_ENTITY,e.getMessage());
        return new ResponseEntity<>(apiError,apiError.getStatus());
    }

    @ExceptionHandler(ProductNotFoundException.class)
    public ResponseEntity<ApiError> handleProductNotFoundException(ProductNotFoundException e){
        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND,e.getMessage());
        return new ResponseEntity<>(apiError,apiError.getStatus());
    }
    @ExceptionHandler(CountryNotFoundException.class)
    public ResponseEntity<ApiError> handleCountryNotFoundException(CountryNotFoundException e){
        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND,e.getMessage());
        return new ResponseEntity<>(apiError,apiError.getStatus());
    }
    @ExceptionHandler(InvalidCountryToDeleteException.class)
    public ResponseEntity<ApiError> handleInvalidCountryToDeleteException(InvalidCountryToDeleteException e){
        ApiError apiError = new ApiError(HttpStatus.CONFLICT,e.getMessage());
        return new ResponseEntity<>(apiError,apiError.getStatus());
    }
}
