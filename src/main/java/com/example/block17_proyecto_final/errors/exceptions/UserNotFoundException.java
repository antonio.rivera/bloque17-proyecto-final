package com.example.block17_proyecto_final.errors.exceptions;

import org.springframework.data.crossstore.ChangeSetPersister;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(String message){
        super(message);
    }
}
