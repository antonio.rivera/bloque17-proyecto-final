package com.example.block17_proyecto_final.errors.exceptions;

public class InvalidCountryToDeleteException extends RuntimeException{
    public InvalidCountryToDeleteException(String message){
        super(message);
    }
}
