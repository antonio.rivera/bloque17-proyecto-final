package com.example.block17_proyecto_final.errors.exceptions;

public class EmailNotValidException extends RuntimeException{
    public EmailNotValidException(String message){
        super(message);
    }
}
