package com.example.block17_proyecto_final.errors.exceptions;

public class InvalidFullNameException extends RuntimeException{
    public InvalidFullNameException(String message){
        super(message);
    }
}
