package com.example.block17_proyecto_final.errors.exceptions;

public class CountryNotFoundException extends RuntimeException {
    public CountryNotFoundException(String message){
        super(message);
    }
}
