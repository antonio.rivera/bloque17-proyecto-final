package com.example.block17_proyecto_final.country.application.services;

import com.example.block17_proyecto_final.country.domain.CountryEntity;

import java.util.List;

public interface CountryService {
    CountryEntity saveCountry(CountryEntity country);

    CountryEntity updateCountry(Integer id, CountryEntity country);

    CountryEntity deleteCountry(Integer id);

    List<CountryEntity> readAllCountries();
}
