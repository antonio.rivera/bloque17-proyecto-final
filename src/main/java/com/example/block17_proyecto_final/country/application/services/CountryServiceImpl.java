package com.example.block17_proyecto_final.country.application.services;

import com.example.block17_proyecto_final.country.domain.CountryEntity;
import com.example.block17_proyecto_final.country.infrastructure.repository.CountryRepository;
import com.example.block17_proyecto_final.errors.exceptions.CountryNotFoundException;
import com.example.block17_proyecto_final.errors.exceptions.InvalidCountryToDeleteException;
import com.example.block17_proyecto_final.user.domain.UserEntity;
import com.example.block17_proyecto_final.user.infrastructure.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CountryServiceImpl implements CountryService{
    private final CountryRepository countryRepository;
    private final UserRepository userRepository;
    @Override
    public CountryEntity saveCountry(CountryEntity country) {
        return countryRepository.save(country);
    }

    @Override
    public CountryEntity updateCountry(Integer id, CountryEntity country) {
        CountryEntity countryToModify = countryRepository.findById(id).orElseThrow(()->new CountryNotFoundException("Country not exists in database"));
        countryToModify.setCode(country.getCode());
        countryToModify.setName(country.getName());
        return countryRepository.saveAndFlush(countryToModify);
    }

    @Override
    public CountryEntity deleteCountry(Integer id) {
        if (userRepository.findAll()
                .stream()
                .filter(userEntity -> !userEntity.isDeleted())
                .filter(userEntity -> userEntity.getCountry().getId().equals(id))
                .collect(Collectors.toList()).size()>0){
            throw new InvalidCountryToDeleteException("You cant delete a country that are assigned to one user");
        }
        CountryEntity countryToDelete = countryRepository.findById(id).orElseThrow(()->new CountryNotFoundException("Country not exists in database"));
        countryRepository.deleteById(id);
        return countryToDelete;
    }

    @Override
    public List<CountryEntity> readAllCountries() {
        return countryRepository.findAll();
    }
}
