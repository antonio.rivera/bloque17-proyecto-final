package com.example.block17_proyecto_final.country.domain;
import com.example.block17_proyecto_final.country.infrastructure.dto.input.CountryInputDto;
import com.example.block17_proyecto_final.country.infrastructure.dto.output.CountryOutputDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;


@Mapper(componentModel = "spring")
public interface CountryMapper {
    CountryMapper INSTANCE = Mappers.getMapper(CountryMapper.class);


    CountryEntity countryInputToEntity(CountryInputDto countryInputDto);

    CountryOutputDto countryEntityToDto(CountryEntity countryEntity);

}
