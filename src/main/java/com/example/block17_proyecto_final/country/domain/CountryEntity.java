package com.example.block17_proyecto_final.country.domain;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Getter
@Setter
//@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "countries")
@Builder
public class CountryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String code;

    private String name;
}
