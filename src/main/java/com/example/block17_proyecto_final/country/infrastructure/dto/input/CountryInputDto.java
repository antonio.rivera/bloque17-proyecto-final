package com.example.block17_proyecto_final.country.infrastructure.dto.input;


import lombok.*;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CountryInputDto {


    private String code;

    private String name;
}
