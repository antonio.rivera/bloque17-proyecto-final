package com.example.block17_proyecto_final.country.infrastructure.repository;

import com.example.block17_proyecto_final.country.domain.CountryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<CountryEntity, Integer> {
    CountryEntity findByCode(String code);
}
