package com.example.block17_proyecto_final.country.infrastructure.controller;


import com.example.block17_proyecto_final.country.application.services.CountryService;
import com.example.block17_proyecto_final.country.domain.CountryEntity;
import com.example.block17_proyecto_final.country.domain.CountryMapper;
import com.example.block17_proyecto_final.country.infrastructure.dto.input.CountryInputDto;

import com.example.block17_proyecto_final.country.infrastructure.dto.output.CountryOutputDto;


import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/country")
@RequiredArgsConstructor
public class CountryController {
    private final CountryService countryService;


    /**
     * Endpoint where we can show all countries
     * ADMIN and USER can access
     * @return ResponseEntity
     */
    @PreAuthorize("hasAuthority('ADMIN')||hasAuthority('USER')")
    @GetMapping()
    public ResponseEntity<List<CountryOutputDto>> findAllCountries(){
        List<CountryOutputDto> countrysOutput =
                countryService.readAllCountries()
                        .stream()
                        .map(country -> CountryMapper.INSTANCE.countryEntityToDto(country))
                        .collect(Collectors.toList());
        return new ResponseEntity<>(countrysOutput, HttpStatus.OK);
    }


    /**
     * Endpoint where we can create country
     * Only ADMIN can access
     * @param countryInputDto
     * @return ResponseEntity
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping()
    public ResponseEntity<CountryOutputDto> saveCountry(@RequestBody CountryInputDto countryInputDto){
        CountryEntity countryEntityInput = CountryMapper.INSTANCE.countryInputToEntity(countryInputDto);
        CountryOutputDto countryOutputDto = CountryMapper.INSTANCE.countryEntityToDto(countryService.saveCountry(countryEntityInput));
        return new ResponseEntity<>(countryOutputDto, HttpStatus.OK);
    }


    /**
     * Endpoint where we can modify country by id
     * Only ADMIN can access
     * @param id
     * @param countryInputDto
     * @return ResponseEntity
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<CountryOutputDto> modifyCountry(@PathVariable Integer id,@RequestBody CountryInputDto countryInputDto){
        CountryEntity country = countryService.updateCountry(id,CountryMapper.INSTANCE.countryInputToEntity(countryInputDto));
        CountryOutputDto countryOutputDto=
                CountryMapper.INSTANCE.countryEntityToDto(country);
        return new ResponseEntity<>(countryOutputDto,HttpStatus.OK);
    }


    /**
     * Endpoint where we can delete country by id
     * Only ADMIN can access
     * @param id
     * @return ResponseEntity
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<CountryOutputDto> deleteCountry(@PathVariable Integer id){
        CountryOutputDto countryOutputDto=
                CountryMapper.INSTANCE.countryEntityToDto(countryService.deleteCountry(id));
        return new ResponseEntity<>(countryOutputDto,HttpStatus.OK);
    }
}
