package com.example.block17_proyecto_final.country.infrastructure.dto.output;

import lombok.*;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CountryOutputDto {

    private Integer id;

    private String code;

    private String name;
}
