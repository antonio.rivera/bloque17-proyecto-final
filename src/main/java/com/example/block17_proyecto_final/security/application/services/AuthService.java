package com.example.block17_proyecto_final.security.application.services;


import com.example.block17_proyecto_final.country.infrastructure.repository.CountryRepository;
import com.example.block17_proyecto_final.security.application.jwt.JwtService;
import com.example.block17_proyecto_final.security.infrastructure.dto.input.LoginRequest;
import com.example.block17_proyecto_final.security.infrastructure.dto.input.RegisterRequest;
import com.example.block17_proyecto_final.security.infrastructure.dto.output.AuthResponse;
import com.example.block17_proyecto_final.user.domain.UserEntity;
import com.example.block17_proyecto_final.user.domain.enums.Role;
import com.example.block17_proyecto_final.user.infrastructure.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Service
@RequiredArgsConstructor
public class AuthService {
    private final UserRepository userRepository;
    private final CountryRepository countryRepository;
    private final JwtService jwtService;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    public AuthResponse login(LoginRequest loginRequest) {
         authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getFullName(),loginRequest.getPassword()));
        UserDetails userDetails = userRepository.findByFullName(loginRequest.getFullName()).orElseThrow();
        String token = jwtService.getToken(userDetails);

        return AuthResponse.builder().token(token).build();
    }

    public AuthResponse register(RegisterRequest registerRequest) {
        UserEntity user = UserEntity.builder()
                .fullName(registerRequest.getFullName())
                .password(passwordEncoder.encode(registerRequest.getPassword()))
                .email(registerRequest.getEmail())
                .country(countryRepository.findByCode(registerRequest.getCountry_code()))
                .createdAt(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))
                .role(Role.USER)
                .build();
        userRepository.save(user);
        return AuthResponse
                .builder()
                .token(jwtService.getToken(user))
                .build();
    }
}
