package com.example.block17_proyecto_final.security.infrastructure.dto.input;


import com.example.block17_proyecto_final.user.domain.enums.Role;
import lombok.*;

import java.util.Date;

@ToString
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterRequest {
    private String fullName;

    private String email;

    private String password;

    private String country_code;

    private Role role;
}
