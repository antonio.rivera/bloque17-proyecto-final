package com.example.block17_proyecto_final.security.infrastructure.dto.input;

import lombok.*;

@ToString
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginRequest {
    private String fullName;
    private String password;
}
