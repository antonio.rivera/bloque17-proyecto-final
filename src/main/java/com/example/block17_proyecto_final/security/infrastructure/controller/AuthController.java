package com.example.block17_proyecto_final.security.infrastructure.controller;

import com.example.block17_proyecto_final.security.application.services.AuthService;
import com.example.block17_proyecto_final.security.infrastructure.dto.input.LoginRequest;
import com.example.block17_proyecto_final.security.infrastructure.dto.input.RegisterRequest;
import com.example.block17_proyecto_final.security.infrastructure.dto.output.AuthResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;
    @PostMapping(value = "login")
    public ResponseEntity<AuthResponse> login(@RequestBody LoginRequest loginRequest){
        return ResponseEntity.ok(authService.login(loginRequest));
    }
    @PostMapping(value = "register")
    public ResponseEntity<AuthResponse> register(@RequestBody RegisterRequest registerRequest){
        return ResponseEntity.ok(authService.register(registerRequest));    }
}
