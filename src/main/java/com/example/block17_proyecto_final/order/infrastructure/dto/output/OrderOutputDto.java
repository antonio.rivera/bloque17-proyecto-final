package com.example.block17_proyecto_final.order.infrastructure.dto.output;

import com.example.block17_proyecto_final.user.infrastructure.dto.output.UserOutputDto;
import lombok.*;

import java.time.LocalDate;


@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderOutputDto {
    private Integer id;

    private String status;

    private LocalDate createdAt;

    private UserOutputDto user;

}
