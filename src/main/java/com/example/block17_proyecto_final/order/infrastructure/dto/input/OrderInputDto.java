package com.example.block17_proyecto_final.order.infrastructure.dto.input;

import lombok.*;

import java.time.LocalDate;
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderInputDto {
    private String status;

    private LocalDate createdAt;

    private Integer user_id;
}
