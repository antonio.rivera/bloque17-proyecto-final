package com.example.block17_proyecto_final.order.infrastructure.controller;


import com.example.block17_proyecto_final.order.application.services.OrderService;
import com.example.block17_proyecto_final.order.domain.OrderEntity;
import com.example.block17_proyecto_final.order.domain.OrderMapper;
import com.example.block17_proyecto_final.order.infrastructure.dto.input.OrderInputDto;
import com.example.block17_proyecto_final.order.infrastructure.dto.output.OrderOutputDto;
import com.example.block17_proyecto_final.user.infrastructure.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
public class OrderController {
    private final OrderService orderService;
    private final UserRepository userRepository;

    /**
     * Endpoint where we can create order
     * ADMIN and USER can access
     * @param orderInputDto
     * @return ResponseEntity
     */
    @PreAuthorize("hasAuthority('ADMIN')||hasAuthority('USER')")
    @PostMapping()
    public ResponseEntity<OrderOutputDto> createOrder(@RequestBody OrderInputDto orderInputDto){
        OrderEntity orderEntity = OrderMapper.INSTANCE.orderInputToEntity(orderInputDto);
        orderEntity.setUser(userRepository.findById(orderInputDto.getUser_id()).get());
        OrderOutputDto orderOutputDto = OrderMapper.INSTANCE.orderEntityToDto(orderService.saveOrder(orderEntity));
        return new ResponseEntity<>(orderOutputDto, HttpStatus.OK);
    }
}
