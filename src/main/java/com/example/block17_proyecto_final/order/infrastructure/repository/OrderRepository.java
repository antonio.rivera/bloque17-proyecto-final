package com.example.block17_proyecto_final.order.infrastructure.repository;

import com.example.block17_proyecto_final.order.domain.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<OrderEntity,Integer> {
}
