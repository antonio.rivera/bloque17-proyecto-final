package com.example.block17_proyecto_final.order.application.services;

import com.example.block17_proyecto_final.order.domain.OrderEntity;
import com.example.block17_proyecto_final.order.infrastructure.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService{

    private final OrderRepository orderRepository;
    @Override
    public OrderEntity saveOrder(OrderEntity order) {
//        if (order.getCreatedAt().after(new Date())){
//            throw new InvalidCreatedDateException("createdAt can not be after than current date");
//        }
        order.setCreatedAt(LocalDate.parse(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))));
        return orderRepository.save(order);
    }
}
