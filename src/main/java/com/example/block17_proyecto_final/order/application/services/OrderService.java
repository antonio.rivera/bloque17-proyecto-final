package com.example.block17_proyecto_final.order.application.services;

import com.example.block17_proyecto_final.order.domain.OrderEntity;

public interface OrderService {
    OrderEntity saveOrder(OrderEntity order);
}
