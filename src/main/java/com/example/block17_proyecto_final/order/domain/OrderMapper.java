package com.example.block17_proyecto_final.order.domain;
import com.example.block17_proyecto_final.order.domain.OrderEntity;
import com.example.block17_proyecto_final.order.infrastructure.dto.input.OrderInputDto;
import com.example.block17_proyecto_final.order.infrastructure.dto.output.OrderOutputDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;


@Mapper(componentModel = "spring")
public interface OrderMapper {
    OrderMapper INSTANCE = Mappers.getMapper(OrderMapper.class);


    @Mapping(source = "user_id", target = "user.id")
    OrderEntity orderInputToEntity(OrderInputDto orderInputDto);

    OrderOutputDto orderEntityToDto(OrderEntity orderEntity);

}
